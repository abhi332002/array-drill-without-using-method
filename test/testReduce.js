//In this file we test our reduce method

const { reduce } = require("../reduce");

//given array 
const items = [1, 2, 3, 4, 5, 5];

//Define test function
function testReduce(arr) {

    //use own reduce function 
    let total = reduce(arr, (acc, curr) => acc + curr, 0);

    //print the sum 
    console.log(total);
}

//call test function 
testReduce(items);