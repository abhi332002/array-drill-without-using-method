//In this file we test our flatten method

const { flatten } = require("../flatten");

//given array 
const nestedArray = [1, [2], [[3]], [[[4]]]];

//Define test function
function testflatten(arr) {

    //use own flatten function 
    let flat = flatten(arr, 3);

    console.log(flat);
}

//call test function 
testflatten(nestedArray);