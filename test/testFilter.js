//In this file we test our filter method

const { filter } = require("../filter");

//given array 
const items = [1, 2, 3, 4, 5, 5];

//Define test function
function testfilter(arr) {

    //use own filter function 
    let even = filter(arr, (element) => element % 2 == 0); //based on condition return element

    //print the sum 
    console.log(even);
}

//call test function 
testfilter(items);