//In this file we test our map method

const { map } = require("../map");

function testMap(arr){

    //we use map method like inbuilt function 
    let double = map(arr,(ele)=> ele*2);                 //double the array value

    //print the array 
    console.log(double);

    //add 2 in every element 
    let addTwo = map(arr, (ele)=> ele+2);

    //print the array 
    console.log(addTwo);

}

//given array 
const items = [1, 2, 3, 4, 5, 5];

//call the function to test map function 
testMap(items);
