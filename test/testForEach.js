//In this file we test our forEach method

const { forEach } = require("../forEach");

//given array 
const items = [1, 2, 3, 4, 5, 5];


//Define test function 
function testForEach(arr) {

    //using function to print the element and index of an array 
    forEach(arr, (ele, indx) => console.log(`Element is ${ele} and element of index is ${indx}`));
}

//call test function here;
testForEach(items);

