//In this file we test our reduce method

const { find } = require("../find");

//given array 
const items = [1, 2, 3, 4, 5, 5];

//Define test function
function testFind(arr, key) {

    //use own find function 
    let number = find(arr, (element) => element > key);

    //print the element 
    console.log(number);
}

//call test function 
testFind(items, 2);