////In this is file we will implement the internal logic of the filter method

// /Define filter function 
function filter(elements, callback) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    let containElements = [];

    //iterate an array 
    for (let index = 0; index < elements.length; index++) {
        if (callback(elements[index], index, elements)) {
            containElements.push(elements[index]);
        }
    }
    return containElements;

}


//exports function 
module.exports = { filter };