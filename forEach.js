//In this file we will implement the internal logic of forEach in built methods 

//Define forEach method
function forEach(elements, callback) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    //iterate on the array 
    for (let index = 0; index < elements.length; index++) {

        //In this callback function we return back the elements, index and array also
        callback(elements[index], index, elements);
    }
}

//export the function 
module.exports = { forEach };