////In this is file we will implement the internal logic of the reduce method

// /Define reduce function 
function reduce(elements, callback, startingValue) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    //check if accumulator is already defined or not 
    let acc = startingValue !== undefined ? startingValue : elements[0];
    let startingIndex = startingValue !== undefined ? 0 : 1;

    // iterate an elements
    for (let index = startingIndex; index < elements.length; index++) {
        acc = callback(acc, elements[index], index, elements);
    }
    return acc;

}


//exports function 
module.exports = { reduce };
