////In this is file we will implement the internal logic of the flatten method


//define function 
function flatten(elements, depth = 1) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    //sotre the flat array 
    let flat = [];

    //recursively open the array 
    function recursiveCall(arr, currDepth) {

        //iterate on each element in array 
        for (let element of arr) {

            if (Array.isArray(element) && currDepth < depth) {
                recursiveCall(element, currDepth + 1);
            } else {
                flat.push(element)
            }
        }
    }
    recursiveCall(elements, 0);
    return flat;

}

//exports function 
module.exports = { flatten };