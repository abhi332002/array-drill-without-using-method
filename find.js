////In this is file we will implement the internal logic of the find method

//define find method 
function find(elements, callback) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    //iterate on array and check condition 
    for (let index = 0; index < elements.length; index++) {
        if (callback(elements[index], index, elements)) {
            return elements[index];
        }
    }

    //if element not found 
    return undefined;
}

//exports function 
module.exports = { find };