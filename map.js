//In this is file we will implement the internal logic of the map method


//Define map function 
function map(elements, callback) {

    //check input 
    if (!Array.isArray(elements) || elements.length < 0) {

        //return the empty array
        console.log("Input is valid, please provide a non empty array");
        return [];
    }

    //store the resultant value in array 
    let containArr = [];

    //interate on array 
    for (let index = 0; index < elements.length; index++) {
        containArr.push(callback(elements[index], index, elements));
    }

    return containArr;

}

//export function 
module.exports = { map };
